import React, { Component } from "react";
import { Grid, Row, Col, Table,DropdownButton,MenuItem } from "react-bootstrap";

import Card from "components/Card/Card.jsx";
import { thArray, tdArray } from "variables/Variables.jsx";
import CandidateDetails from "./CandidateDetails.jsx";
import * as d3 from "d3";
import $ from "jquery";

const divButton= {
  display: "inline-block",
  color:"#444",
  border:"1px solid #CCC",
  background:"#DDD",
  cursor:"pointer",
  padding: "5px",
};

var positions={
1:"JR",
2:"SE",
3:"SSE",
3:"LSE",
3:"ATM",
3:"TM"
};

class TableList extends Component {
  constructor(props){
    super( props );
    this.hideSVG= this.hideSVG.bind(this);
    this.rowClicked= this.rowClicked.bind(this);
    this.showCandidateDetails= this.showCandidateDetails.bind(this);
    this.handleChildClick=this.handleChildClick.bind(this);
    this.handlePositionSelect=this.handlePositionSelect.bind(this);
    this.bvalue="SHOW";
    this.state = {
      floatValue:"right",
      displayValue:"none",
      candidateName:"",
      candidatePosition:"",
      candidateContact:"",
      candidateExperience:"",
      candidateTechnology:"",
      buttonValue:this.bvalue,
      selectedPostion:"Position"
    }
  }
  componentDidMount(){
    this.hideSVG();
  }
  hideSVG() {
    var elements = document.querySelectorAll("svg");
    if(elements !=undefined && elements != null){
      [].forEach.call(elements, function(element) {
        element.style.display = "none";
      });
    }
  }

  showCandidateDetails(){
    if(this.state.displayValue=="none"){
      this.setState({ displayValue: "block",floatValue:"center",RowbuttonValue:"CLOSE" });
    }else{
      this.setState({ displayValue: "none",floatValue:"right",RowbuttonValue:"" });
    }
  }

  rowClicked(prop){
    this.setState({ candidateName: prop[2] ,
      candidatePosition:prop[1],
      candidateContact:prop[3],
      candidateExperience:prop[4],
      candidateTechnology:prop[5],
      displayValue:"block",float:"center",RowbuttonValue:"CLOSE"});

     // $("#candidateDetailsClose").html("<i className='pe-7s-close text-warning' />");
  }

  handleChildClick(e) {
    e.stopPropagation();
  }

  handlePositionSelect(evt,evtKey){
    this.setState({selectedPostion:positions[evt]});
  }


  render() {
    return (

      <div className="content">
      <div id="candidateDetailsClose" className={divButton} onClick={this.showCandidateDetails} >{this.state.RowbuttonValue} </div>
      <CandidateDetails
        display={this.state.displayValue}
        float={this.state.floatValue}
        candidateName={this.state.candidateName}
        candidatePosition={this.state.candidatePosition}
        candidateContact={this.state.candidateContact}
        candidateExperience={this.state.candidateExperience}
        candidateTechnology={this.state.candidateTechnology}
        />
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card
                title="Candidates"
                category=""
                ctTableFullWidth
                ctTableResponsive
                content={
                  <Table striped hover>
                    <thead>
                      {/* <tr>
                        {thArray.map((prop, key) => {
                          return <th key={key}>{prop}</th>;
                        })}
                      </tr> */}
                      <tr>
                            <th key="0">ID</th>
                            <th key="1">Position</th>
                            <th key="2">NAME</th>
                            <th key="3">CONTACT</th>
                            <th key="4">EXPERIENCE</th>
                            <th key="5">TECHNOLOGY</th>
                            <th key="6">AVAILABILITY</th>
                            <th key="7">INERVIEW STATUS</th>
                      </tr>
                    </thead>
                    <tbody>
                      {
                        tdArray.map((prop, key) => {
                        return (
                          <tr onClick={ () => this.rowClicked(prop)} key={key}>
                            {prop.map((prop, key) => {
                                   if(key !=1){
                                    return <td   key={key}>{prop}</td>;
                                  }
                                  else{
                                  return <td key={key}>
                                  <div onClick={this.handleChildClick}>
                                      <DropdownButton onSelect={(evt,evtKey)=> this.handlePositionSelect(evt,evtKey)}
                                            bsStyle=""
                                            title="Position"
                                            key={key}
                                            id="test"
                                          >
                                            <MenuItem eventKey="1">JR - SE</MenuItem>
                                            <MenuItem eventKey="2">SE </MenuItem>
                                            <MenuItem eventKey="3">SSE</MenuItem>
                                            <MenuItem eventKey="3">LSE</MenuItem>
                                            <MenuItem eventKey="3">ATM</MenuItem>
                                            <MenuItem eventKey="3">TM</MenuItem>
                                          </DropdownButton>
                                    </div>
                                        </td>;
                                  }
                            })}
                          </tr>
                        );
                      })}
                    </tbody>
                  </Table>
                }
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default TableList;
