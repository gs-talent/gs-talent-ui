import React, { Component } from "react";
import ChartistGraph from "react-chartist";
import { Grid, Row, Col } from "react-bootstrap";

import { Card } from "components/Card/Card.jsx";
import { StatsCard } from "components/StatsCard/StatsCard.jsx";
import { Tasks } from "components/Tasks/Tasks.jsx";
import {
  dataPie,
  legendPie,
  dataSales,
  optionsSales,
  responsiveSales,
  legendSales,
  dataBar,
  optionsBar,
  responsiveBar,
  legendBar
} from "variables/Variables.jsx";

import TreeChart from "views/D3Tree.jsx";
//import ClusteredLayoutChart1 from "views/D3Clusteredlayout1.jsx";
import ClusteredLayoutChart from "views/D3Clusteredlayout.jsx";
//import ZoomSunbusrtChart from "views/D3ZoomSunburst.jsx";

class Dashboard extends Component {

  static BASEURL = "https://jsonplaceholder.typicode.com"; 
  
  static TYPEOFPROFILE = {
    UPLOADEDPROFILES:"UPLOADEDPROFILES",
    SHORTLISTEDPROFILES:"SHORTLISTEDPROFILES",
    TECHINPROGRESS:"TECHINPROGRESS",
    MANAGERINPROGRESS:"MANAGERINPROGRESS",
    HRINPROGRESS:"HRINPROGRESS",
    OFFEREDPROFILES:"OFFEREDPROFILES",
    REJECTEDPROFILES:"REJECTEDPROFILES",
    GSONBOARDPROFILES:"GSONBOARDPROFILES",
  }

  constructor(props) {
    super(props);
    this.getCounts = this.getCounts.bind(this);
    this.getSpecificCount = this.getSpecificCount.bind(this);
    this.showSVG= this.showSVG.bind(this);
    this.state = {
      uploadedProfilesCount: "NA",
      shortlistedProfilesCount: "NA",
      techInProgressCount: "NA",
      managerInProgressCount: "NA",
      hrInProgressCount: "NA",
      offeredProfileCount: "NA",
      rejectedProfileCount: "NA",
      gsOnBoardCount: "NA",
    };
    this.getCounts();
    this.showSVG();
  }

  showSVG() {
    var elements = document.querySelectorAll("svg");
    if(elements !=undefined && elements != null){
      [].forEach.call(elements, function(element) {
        element.style.display = "block";
      });
      
    }
  }

  getSpecificCount(URL, typeOfCount){
    fetch(URL)
      .then(res => res.json())
        .then(
          (result) => {
            if(typeOfCount === Dashboard.TYPEOFPROFILE.UPLOADEDPROFILES){
                this.setState({
                uploadedProfilesCount: result.userId
              });
            } else if(typeOfCount === Dashboard.TYPEOFPROFILE.SHORTLISTEDPROFILES){
              this.setState({
                 shortlistedProfilesCount:  result.userId
              });
            }else if(typeOfCount === Dashboard.TYPEOFPROFILE.TECHINPROGRESS){
              this.setState({
                 techInProgressCount: result.userId
              });
            }else if(typeOfCount === Dashboard.TYPEOFPROFILE.MANAGERINPROGRESS){
              this.setState({
                 managerInProgressCount:  result.userId
              });
            }else if(typeOfCount === Dashboard.TYPEOFPROFILE.HRINPROGRESS){
              this.setState({
                 hrInProgressCount: result.userId
              });
            }else if(typeOfCount === Dashboard.TYPEOFPROFILE.OFFEREDPROFILES){
              this.setState({
                 offeredProfileCount:  result.userId
              });
            }else if(typeOfCount === Dashboard.TYPEOFPROFILE.REJECTEDPROFILES){
              this.setState({
                 rejectedProfileCount:  result.userId
              });
            }else if(typeOfCount === Dashboard.TYPEOFPROFILE.GSONBOARDPROFILES){
              this.setState({
                 gsOnBoardCount:  result.userId
              });
            }


          },
          (error) => {
            this.setState({
              uploadedProfilesCount: 0,
              shortlistedProfilesCount:  0,
              techInProgressCount: 0,
              managerInProgressCount:  0,
              hrInProgressCount:  0,
              offeredProfileCount: 0,
              rejectedProfileCount:  0,
              gsOnBoardCount:  0
            });
          }
      )
  }

  getCounts(){
    this.getSpecificCount(Dashboard.BASEURL+"/todos/1",Dashboard.TYPEOFPROFILE.UPLOADEDPROFILES);
    this.getSpecificCount(Dashboard.BASEURL+"/todos/1",Dashboard.TYPEOFPROFILE.SHORTLISTEDPROFILES);
    this.getSpecificCount(Dashboard.BASEURL+"/todos/1",Dashboard.TYPEOFPROFILE.TECHINPROGRESS);
    this.getSpecificCount(Dashboard.BASEURL+"/todos/1",Dashboard.TYPEOFPROFILE.MANAGERINPROGRESS);
    this.getSpecificCount(Dashboard.BASEURL+"/todos/1",Dashboard.TYPEOFPROFILE.HRINPROGRESS);
    this.getSpecificCount(Dashboard.BASEURL+"/todos/1",Dashboard.TYPEOFPROFILE.OFFEREDPROFILES);
    this.getSpecificCount(Dashboard.BASEURL+"/todos/1",Dashboard.TYPEOFPROFILE.REJECTEDPROFILES);
    this.getSpecificCount(Dashboard.BASEURL+"/todos/1",Dashboard.TYPEOFPROFILE.GSONBOARDPROFILES);
  }
  
  createLegend(json) {
    var legend = [];
    for (var i = 0; i < json["names"].length; i++) {
      var type = "fa fa-circle text-" + json["types"][i];
      legend.push(<i className={type} key={i} />);
      legend.push(" ");
      legend.push(json["names"][i]);
    }
    return legend;
  }
  render() {
    return (
      <div id = "dashboardCharts" className="content">
        <Grid fluid>
          <Row>
            <Col md={8}>
              <div id="treechartdiv1">
                <TreeChart />
              </div>
            </Col>
          </Row>
          <Row>
            <Col md={8}>
              <div id="treechartdiv3">
                <ClusteredLayoutChart />
              </div>
            </Col>
          </Row>
          <Row>
          <Col lg={3} sm={6}>
              <StatsCard
                bigIcon={<i className="pe-7s-wallet text-success" />}
                statsText="Uploaded Profiles"
                statsValue={this.state.uploadedProfilesCount}
                statsIcon={<i className="fa fa-calendar-o" />}
                statsIconText="Last day"
              />
            </Col>
            <Col lg={3} sm={6}>
              <StatsCard
                bigIcon={<i className="pe-7s-server text-warning" />}
                statsText="Shortlisted Profiles"
                statsValue={this.state.shortlistedProfilesCount}
                statsIcon={<i className="fa fa-refresh" />}
                statsIconText="Updated now"
              />
            </Col>
            <Col lg={3} sm={6}>
              <StatsCard
                bigIcon={<i className="pe-7s-refresh-2 text-success" />}
                statsText="Tech - In Progress"
                statsValue={this.state.techInProgressCount}
                statsIcon={<i className="fa fa-clock-o" />}
                statsIconText="In the last hour"
              />
            </Col>
            <Col lg={3} sm={6}>
              <StatsCard
                bigIcon={<i className="pe-7s-refresh-2 text-success" />}
                statsText="Manager - In Progress"
                statsValue={this.state.managerInProgressCount}
                statsIcon={<i className="fa fa-refresh" />}
                statsIconText="Updated now"
              />
            </Col>
          </Row>
          <Row>
          <Col lg={3} sm={6}>
              <StatsCard
                bigIcon={<i className="pe-7s-refresh-2 text-success" />}
                statsText="HR - In Progress"
                statsValue={this.state.hrInProgressCount}
                statsIcon={<i className="fa fa-clock-o" />}
                statsIconText="In the last hour"
              />
            </Col>
          <Col lg={3} sm={6}>
              <StatsCard
                bigIcon={<i className="pe-7s-wallet text-success" />}
                statsText="Offered"
                statsValue={this.state.offeredProfileCount}
                statsIcon={<i className="fa fa-calendar-o" />}
                statsIconText="Last day"
              />
            </Col>
            <Col lg={3} sm={6}>
              <StatsCard
                bigIcon={<i className="pe-7s-delete-user text-warning" />}
                statsText="Rejected"
                statsValue={this.state.rejectedProfileCount}
                statsIcon={<i className="fa fa-refresh" />}
                statsIconText="Updated now"
              />
            </Col>
           
            <Col lg={3} sm={6}>
              <StatsCard
                bigIcon={<i className="pe-7s-smile text-success" />}
                statsText="GS Onboard"
                statsValue={this.state.gsOnBoardCount}
                statsIcon={<i className="fa fa-refresh" />}
                statsIconText="Updated now"
              />
            </Col>
          </Row>

          {/* <Row>
            <Col md={8}>
              <Card
                statsIcon="fa fa-history"
                id="chartHours"
                title="Users Behavior"
                category="24 Hours performance"
                stats="Updated 3 minutes ago"
                content={
                  <div className="ct-chart">
                    <ChartistGraph
                      data={dataSales}
                      type="Line"
                      options={optionsSales}
                      responsiveOptions={responsiveSales}
                    />
                  </div>
                }
                legend={
                  <div className="legend">{this.createLegend(legendSales)}</div>
                }
              />
            </Col>
            <Col md={4}>
              <Card
                statsIcon="fa fa-clock-o"
                title="Email Statistics"
                category="Last Campaign Performance"
                stats="Campaign sent 2 days ago"
                content={
                  <div
                    id="chartPreferences"
                    className="ct-chart ct-perfect-fourth"
                  >
                    <ChartistGraph data={dataPie} type="Pie" />
                  </div>
                }
                legend={
                  <div className="legend">{this.createLegend(legendPie)}</div>
                }
              />
            </Col>
          </Row> */}

          {/* <Row>
            <Col md={6}>
              <Card
                id="chartActivity"
                title="2014 Sales"
                category="All products including Taxes"
                stats="Data information certified"
                statsIcon="fa fa-check"
                content={
                  <div className="ct-chart">
                    <ChartistGraph
                      data={dataBar}
                      type="Bar"
                      options={optionsBar}
                      responsiveOptions={responsiveBar}
                    />
                  </div>
                }
                legend={
                  <div className="legend">{this.createLegend(legendBar)}</div>
                }
              />
            </Col>

            <Col md={6}>
              <Card
                title="Tasks"
                category="Backend development"
                stats="Updated 3 minutes ago"
                statsIcon="fa fa-history"
                content={
                  <div className="table-full-width">
                    <table className="table">
                      <Tasks />
                    </table>
                  </div>
                }
              />
            </Col>
          </Row> */}
        </Grid>
      </div>
    );
  }
}

export default Dashboard;
