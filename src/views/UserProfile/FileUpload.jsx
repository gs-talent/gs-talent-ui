import React from 'react';
import FileDrop from 'react-file-drop';

const styles = { border: '1px solid black', width: 600, color: 'black', padding: 20, height:200 };

class ReactFileDropDemo extends React.Component {

  constructor(props) {
    super(props);
    this.filenames= "";
    this.handleDrop = this.handleDrop.bind(this);
}

  handleDrop = (files, event) => {
    debugger
    
    for(var i=0;i<files.length;i++) {
      console.log(files[i].name);
      var name = files[i].name+"\n";
      this.filenames = this.filenames + name;
    }
    alert(this.filenames +"\n Uploaded successfully");
  }

  render() {
    return (
      <div id="react-file-drop-demo" style={styles}>
        <FileDrop onDrop={this.handleDrop}>
          Drop Resumes here!
        </FileDrop>
      </div>
    );
  }
}

export default ReactFileDropDemo;