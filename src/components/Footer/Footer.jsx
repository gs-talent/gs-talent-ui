import React, { Component } from "react";
import { Grid } from "react-bootstrap";

class Footer extends Component {
  render() {
    return (
      <footer className="footer">
        <Grid fluid>
          <nav className="pull-left">
            <ul>
              <li>
                <a href="https://www.gslab.com/about-us/">Company</a>
              </li>
              <li>
                <a href="https://www.gslab.com/case-studies/">Portfolio</a>
              </li>
              <li>
                <a href="https://www.gslab.com/blogs/">Blog</a>
              </li>
            </ul>
          </nav>
          <p className="copyright pull-right">
            &copy; {new Date().getFullYear()}{" "}
            <a href="http://www.gslab.com"> GSLab</a>
          </p>
        </Grid>
      </footer>
    );
  }
}

export default Footer;
