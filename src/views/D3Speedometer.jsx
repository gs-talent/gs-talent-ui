import React, {Component} from 'react';
import ReactSpeedometer from "react-d3-speedometer";

class SpeedoMeterChart extends Component {

    render(){
        return <div >
            <div style={{width: "500px", height: "300px", background: "#EFEFEF"}}>
                <ReactSpeedometer
                    fluidWidth
                    minValue={100}
                    maxValue={500}
                    value={473}
                />
            </div>
        </div>
      }
}

export default SpeedoMeterChart;
