import React, { Component } from "react";
import {  Row, Col, Table,DropdownButton,MenuItem } from "react-bootstrap";

import Card from "components/Card/Card.jsx";
//import { thArray1, tdArray1 } from "variables/Variables.jsx";
import $ from "jquery";
const thArray1 = ["level", "Panel","Mode", "Scheduled Date Time","Status","Feedback"];
const tdArray1 = [
  ["Level 1 - Tech", "Anand Sonake","Telephonic","15-01-2019 T 11:00 AM",   "Select","Good"],
  ["Level 2 - Manager", "Anand Joshi","Telephonic","15-01-2019 T 11:00 AM","Reject","Below Average"],
  ["Level 1 - HR", "Test","Telephonic","15-01-2019 T 11:00 AM","888428843","",""]  
];



class CandidateDetails extends Component {

  constructor(props){
    super( props );
    this.state= {
      display: props.display,
      float: props.float,
      data:"",
      candidateName:"",
      candidatePosition:"",
      candidateContact:"",
      candidateExperience:"",
      candidateTechnology:""
    }
  }

  componentDidMount(){
  }
  componentWillReceiveProps(nextProps){
    // if(nextProps.display=="block")
    //   $("#CandidatesDetails").show(2000 );
    // else
    //   $("#CandidatesDetails").hide(2000 );

      this.setState({
        display: nextProps.display,
        float:nextProps.float,
        candidateName: nextProps.candidateName,
        candidatePosition:nextProps.candidatePosition,
        candidateContact:nextProps.candidateContact,
        candidateExperience:nextProps.candidateExperience,
        candidateTechnology:nextProps.candidateTechnology
      });

  }
  componentDidUpdate(){
    
    
  }

  render() {
    return (
      <div style={{"width": "100%","height":"300px","display":this.state.display}}>
      <div id="CandidatesDetails" style={{"width": "50%","height":"450px","display":this.state.display, "border":"3px solid GREY","margin": "0 auto"}}>
      <div style={{"width": "50%","margin":"0 auto","height":"100px"}}>
      <Row>
        <Col md={12} mdOffset={2}>
             <h3> {this.state.candidateName}</h3>
        </Col>
        </Row>
        <Row>
        <Col md={3}>
          <h4> {this.state.candidatePosition}</h4>
        </Col>
        <Col Col md={3}>
        <h4>  {this.state.candidateContact}</h4>
        </Col>
        {/* <Col md={3}>              
              {this.state.candidateExperience}
        </Col> */}
        <Col md={3}>
        <h4>    {this.state.candidateTechnology}</h4>
        </Col>
      </Row>
      </div>
             
              <Row>
                <Col md={12}>
                <Card
                title="Candidate Details"
                category=""
                ctTableFullWidth
                ctTableResponsive
                content={
                  <Table striped hover>
                    <thead>
                      {/* <tr>
                        {                          
                          thArray1.map((prop, key) => {
                          return <th key={key}>{prop}</th>;
                        })}
                      </tr> */}
                       <tr>
                            <th key="0">LEVEL</th>
                            <th key="1">PANEL</th>
                            <th key="2">MODE</th>
                            <th key="3">SCHEDULED DATE TIME</th>
                            <th key="4">STATUS</th>
                            <th key="5">FEEDBACK</th>
                      </tr>
                    </thead>
                    <tbody>
                      {
                        tdArray1.map((prop, key) => {
                        return (
                          <tr>
                            {prop.map((prop, key) => {
                              if(key ==0){
                                return <td key={key}>
                                <DropdownButton
                                      bsStyle=""
                                      title="Level"
                                      key={key}
                                      id="test"
                                    >
                                      <MenuItem eventKey="1">Level 1 - Tech</MenuItem>
                                      <MenuItem eventKey="2">Level 2 - Manager</MenuItem>
                                      <MenuItem eventKey="3">Level 3 - HR</MenuItem>
                                    </DropdownButton>
                                  </td>;
                              }else if(key==2){
                                return <td key={key}>
                                <DropdownButton
                                      bsStyle=""
                                      title="Mode"
                                      key={key}
                                      id="test"
                                    >
                                      <MenuItem eventKey="1">Telephonic</MenuItem>
                                      <MenuItem eventKey="2">F2F</MenuItem>
                                      <MenuItem eventKey="3">Skype</MenuItem>
                                    </DropdownButton>
                                  </td>;
                              }else if(key==4){
                                return <td key={key}>
                                <DropdownButton
                                      bsStyle=""
                                      title="Status"
                                      key={key}
                                      id="test"
                                    >
                                      <MenuItem eventKey="1">select</MenuItem>
                                      <MenuItem eventKey="2">Reject</MenuItem>
                                    </DropdownButton>
                                  </td>;
                              }
                              else {
                                return <td   key={key}>{prop}</td>;
                              
                              }
                            })}
                          </tr>
                        );
                      })}
                    </tbody>
                  </Table>
                }
              />
                </Col>
              </Row>
      </div>
      </div>
      
    );
  }
}

export default CandidateDetails;
