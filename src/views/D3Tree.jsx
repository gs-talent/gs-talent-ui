import React, {Component} from 'react';
import * as d3 from "d3";

var treeData =
  {
    "name": "Technologies",
    "children": [
      { 
        "name": ".Net",
        "children": [
          { "name": "Anand Sonake",
			"children": [
			  { "name": "+91 8888428842" },
			  { "name": "anandsonake@gmail.com" }
			]
		  },
          { "name": "Ishwar Patil" }
        ]
      },
      { "name": "Java" ,
		"children": [
          { "name": "Anand Joshi" }          
        ]
	  
	  },
	  { "name": "C" },
	  { "name": "C++" },
	  { "name": "Java" }
    ]
  };

var margin = {top: 0, right: 90, bottom: 30, left: 400},
    width = 960 ,
    height = 500 - margin.top - margin.bottom;

var svg = d3.select("body").append("svg")
    .attr("width", width + margin.right + margin.left)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate("
          + margin.left + "," + margin.top + ")");

var i = 0,
    duration = 750,
    root;

var treemap = d3.tree().size([height, width]);


class TreeChart extends Component {

    constructor(props){
        super( props );
        this.collapse= this.collapse.bind(this);
        this.click= this.click.bind(this);
        this.diagonal= this.diagonal.bind(this);

    }
    componentDidMount() {
        this.drawChart();
      }

      drawChart() {
        console.log("drwan");
        root = d3.hierarchy(treeData, function(d) { return d.children; });
        root.x0 = height / 2;
        root.y0 = 0;
        root.children.forEach(this.collapse);
        this.update(root);
    }

    collapse(d) {
    if(d.children) {
      d._children = d.children
      d._children.forEach(this.collapse)
      d.children = null
    }
  }
  
  update(source) {
  
    var treeData = treemap(root);
  
    var nodes = treeData.descendants(),
        links = treeData.descendants().slice(1);
  
    nodes.forEach(function(d){ d.y = d.depth * 180});
  
    var node = svg.selectAll('g.node')
        .data(nodes, function(d) {return d.id || (d.id = ++i); });
  
    var nodeEnter = node.enter().append('g')
        .attr('class', 'node')
        .attr("transform", function(d) {
          return "translate(" + source.y0 + "," + source.x0 + ")";
      })
      .on('click', this.click);
  
    nodeEnter.append('circle')
        .attr('class', 'node')
        .attr('r', 1e-6)
        .style("fill", function(d) {
            return d._children ? "lightsteelblue" : "GREEN";
        });
  
    nodeEnter.append('text')
        .attr("dy", ".25em")
        .attr("x", function(d) {
            return d.children || d._children ? -13 : 13;
        })
        .attr("text-anchor", function(d) {
            return d.children || d._children ? "end" : "start";
        })
        .text(function(d) { return d.data.name; });
  
    var nodeUpdate = nodeEnter.merge(node);
  
    nodeUpdate.transition()
      .duration(duration)
      .attr("transform", function(d) { 
          return "translate(" + d.y + "," + d.x + ")";
       });
  
    nodeUpdate.select('circle.node')
      .attr('r', 10)
      .style("fill", function(d) {
          return d._children ? "lightsteelblue" : "#FFA07A";
      })
      .attr('cursor', 'pointer');
  
  
    var nodeExit = node.exit().transition()
        .duration(duration)
        .attr("transform", function(d) {
            return "translate(" + source.y + "," + source.x + ")";
        })
        .remove();
  
    nodeExit.select('circle')
      .attr('r', 1e-6);
  
    nodeExit.select('text')
      .style('fill-opacity', 1e-6);
  
    var link = svg.selectAll('path.link')
        .data(links, function(d) { return d.id; });

    var that=this;
    var linkEnter = link.enter().insert('path', "g")
        .attr("class", "link")
        .attr('d', function(d){
          var o = {x: source.x0, y: source.y0}
          return that.diagonal(o, o)
        });
  
    var linkUpdate = linkEnter.merge(link);
  
    linkUpdate.transition()
        .duration(duration)
        .attr('d', function(d){ return that.diagonal(d, d.parent) });
  
    var linkExit = link.exit().transition()
        .duration(duration)
        .attr('d', function(d) {
          var o = {x: source.x, y: source.y}
          return that.diagonal(o, o)
        })
        .remove();
  
    nodes.forEach(function(d){
      d.x0 = d.x;
      d.y0 = d.y;
    });
  }

  diagonal(s, d) {
  
    return  `M ${s.y} ${s.x}
            C ${(s.y + d.y) / 2} ${s.x},
              ${(s.y + d.y) / 2} ${d.x},
              ${d.y} ${d.x}`
  }

  click(d) {
    if (d.children) {
        d._children = d.children;
        d.children = null;
      } else {
        d.children = d._children;
        d._children = null;
      }
    this.update(d);
  }

    render(){
        return <div id={"#" + this.props.id}></div>
      }
}

export default TreeChart;