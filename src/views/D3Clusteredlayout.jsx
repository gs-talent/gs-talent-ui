import React, {Component} from 'react';
import { forceAttract } from 'd3-force-attract';
import { forceCluster } from 'd3-force-cluster';
import * as d3 from "d3";



var nodes = [
	{
		cluster: 1,
		radius: 5,
		x:Math.cos(1 / m * 2 * Math.PI) * 200 + width / 2 + Math.random(),
		y:Math.sin(1 / m * 2 * Math.PI) * 200 + height / 2 + Math.random(),
		name: "Ishwar patil",
		number: "+91 8888428843"
  },
  {
		cluster: 1,
		radius: 8,
		x:Math.cos(1 / m * 2 * Math.PI) * 200 + width / 2 + Math.random(),
		y:Math.sin(1 / m * 2 * Math.PI) * 200 + height / 2 + Math.random(),
		name: "Ishwar patil",
		number: "+91 8888428843"
  },
  {
		cluster: 1,
		radius: 3,
		x:Math.cos(1 / m * 2 * Math.PI) * 200 + width / 2 + Math.random(),
		y:Math.sin(1 / m * 2 * Math.PI) * 200 + height / 2 + Math.random(),
		name: "Ishwar patil",
		number: "+91 8888428843"
  },
  {
		cluster: 1,
		radius: 12,
		x:Math.cos(1 / m * 2 * Math.PI) * 200 + width / 2 + Math.random(),
		y:Math.sin(1 / m * 2 * Math.PI) * 200 + height / 2 + Math.random(),
		name: "Ishwar patil",
		number: "+91 8888428843"
	},
	{
		cluster: 2,
		radius: 2,
		x:Math.cos(2 / m * 2 * Math.PI) * 200 + width / 2 + Math.random(),
		y:Math.sin(2 / m * 2 * Math.PI) * 200 + height / 2 + Math.random(),
		name: "Test User2",
		number: "+91 8888428843"
  },
  {
		cluster: 2,
		radius: 4,
		x:Math.cos(2 / m * 2 * Math.PI) * 200 + width / 2 + Math.random(),
		y:Math.sin(2 / m * 2 * Math.PI) * 200 + height / 2 + Math.random(),
		name: "Test User2",
		number: "+91 8888428843"
  },
  {
		cluster: 2,
		radius: 6,
		x:Math.cos(2 / m * 2 * Math.PI) * 200 + width / 2 + Math.random(),
		y:Math.sin(2 / m * 2 * Math.PI) * 200 + height / 2 + Math.random(),
		name: "Test User2",
		number: "+91 8888428843"
  },
  {
		cluster: 2,
		radius: 8,
		x:Math.cos(2 / m * 2 * Math.PI) * 200 + width / 2 + Math.random(),
		y:Math.sin(2 / m * 2 * Math.PI) * 200 + height / 2 + Math.random(),
		name: "Test User2",
		number: "+91 8888428843"
	},
	{
		cluster: 3,
		radius: 12,
		x:Math.cos(3 / m * 2 * Math.PI) * 200 + width / 2 + Math.random(),
		y:Math.sin(3 / m * 2 * Math.PI) * 200 + height / 2 + Math.random(),
		name: "Anand Sonake",
		number: "+91 8888428843"
	},
	{
		cluster: 4,
		radius: 2,
		x:Math.cos(4 / m * 2 * Math.PI) * 200 + width / 2 + Math.random(),
		y:Math.sin(4 / m * 2 * Math.PI) * 200 + height / 2 + Math.random(),
		name: "Test User",
		number: "+91 8888428843"
	},
	{
		cluster: 5,
		radius: 12,
		x:Math.cos(5 / m * 2 * Math.PI) * 200 + width / 2 + Math.random(),
		y:Math.sin(5 / m * 2 * Math.PI) * 200 + height / 2 + Math.random(),
		name: "Rahul Sahasrabuddhe",
		number: "+91 8888428843"
  },
  {
		cluster: 5,
		radius: 5,
		x:Math.cos(5 / m * 2 * Math.PI) * 200 + width / 2 + Math.random(),
		y:Math.sin(5 / m * 2 * Math.PI) * 200 + height / 2 + Math.random(),
		name: "Rahul Sahasrabuddhe",
		number: "+91 8888428843"
  },
  {
		cluster: 5,
		radius: 9,
		x:Math.cos(5 / m * 2 * Math.PI) * 200 + width / 2 + Math.random(),
		y:Math.sin(5 / m * 2 * Math.PI) * 200 + height / 2 + Math.random(),
		name: "Rahul Sahasrabuddhe",
		number: "+91 8888428843"
	},
	{
		cluster: 1,
		radius: 8,
		x:Math.cos(1 / m * 2 * Math.PI) * 200 + width / 2 + Math.random(),
		y:Math.sin(1 / m * 2 * Math.PI) * 200 + height / 2 + Math.random(),
		name: "Anand Joshi",
		number: "+91 8888428843"
	}
];

var div = d3.select("body").append("div")	
        .attr("class", "tooltip")				
        .style("opacity", 0);

var width = 960,
    height = 500,
    padding = 1.5, // separation between same-color nodes
    clusterPadding = 6, // separation between different-color nodes
    maxRadius = 12;

var n = 100, // total number of nodes
    m = 5; // number of distinct clusters

// var color = d3.scale.category10()
var color = d3.scaleSequential(d3.interpolateRainbow)
    .domain(d3.range(m));

// The largest node for each cluster.
var clusters = new Array(m);


var simulation = d3.forceSimulation()
.nodes(nodes)
  // keep entire simulation balanced around screen center
  .force('center', d3.forceCenter(width/2, height/2))
  .force('attract', forceAttract()
  .target([width/2, height/2])
  .strength(0.01))
  .force('cluster', forceCluster()
    .centers(function (d) { return clusters[d.cluster]; })
    .strength(0.5)
    .centerInertia(0.1))
    .force('collide', d3.forceCollide(function (d) { return d.radius + padding; })
    .strength(0))
  .on('tick', layoutTick)
  ;
 
  var svg =null;

  var node = null;


    function dragstarted (d) {
      if (!d3.event.active) simulation.alphaTarget(0.3).restart();
      d.fx = d.x;
      d.fy = d.y;
    }
    
    function dragged (d) {
      d.fx = d3.event.x;
      d.fy = d3.event.y;
    }
    
    function dragended (d) {
      if (!d3.event.active) simulation.alphaTarget(0);
      d.fx = null;
      d.fy = null;
    }

    // ramp up collision strength to provide smooth transition
var transitionTime = 3000;
var t = d3.timer(function (elapsed) {
  var dt = elapsed / transitionTime;
  simulation.force('collide').strength(Math.pow(dt, 2) * 0.7);
  if (dt >= 1.0) t.stop();
});
  
function layoutTick (e) {
  if(node!=null && node !=undefined){
  node
    .attr('cx', function (d) { return d.x; })
    .attr('cy', function (d) { return d.y; })
    .attr('r', function (d) { return d.radius; });
  }
}


class ClusteredLayoutChart extends Component {

    constructor(props){
        super( props );
    }
    componentDidMount() {
        if(document.querySelector('body > svg > circle') === undefined || document.querySelector('body > svg > circle') === null){
          this.drawChart();
        }
      }

      drawChart() {
        svg = d3.select('body').append('svg')
        .attr('width', width)
        .attr('height', height);
      
        node = svg.selectAll('circle')
        .data(nodes)
        .enter().append('circle')
          .style('fill', function (d) { return color(d.cluster/10); })
          .on('mouseover',function(d) {
            d3.select(this)
              .transition()
              .duration(1000)
              .attr('stroke-width',3);
            div.transition()		
                .duration(200)		
                .style("opacity", .9);		
            div.html(d.name+"</br>"+d.number)	
                .style("left", (d3.event.pageX +60) + "px")		
                .style("top", (d3.event.pageY - 30) + "px");	 
          })
          .on('mouseout',function () {
            d3.select(this)
              .transition()
              .duration(1000)
              .attr('stroke-width',0);
            div.transition()		
                .duration(500)		
                .style("opacity", 0);	
          })
          .call(d3.drag()
            .on('start', dragstarted)
            .on('drag', dragged)
            .on('end', dragended)

          );
    }
  
    render(){
        return <div id={"#" + this.props.id}></div>
      }

}

export default ClusteredLayoutChart;