import React, { Component } from "react";
import {
  Grid,
  Row,
  Col,
  FormGroup,
  ControlLabel,
  FormControl
} from "react-bootstrap";

import { Card } from "components/Card/Card.jsx";
import { FormInputs } from "components/FormInputs/FormInputs.jsx";
import { UserCard } from "components/UserCard/UserCard.jsx";
import Button from "components/CustomButton/CustomButton.jsx";

import avatar from "assets/img/faces/face-3.jpg";
import FileUpload from './FileUpload.jsx';

class UserProfile extends Component {
  constructor(props){
    super( props );
    this.hideSVG= this.hideSVG.bind(this);
  }

  componentDidMount(){
    this.hideSVG();
  }
  hideSVG() {
    var elements = document.querySelectorAll("svg");
    if(elements !=undefined && elements != null){
      [].forEach.call(elements, function(element) {
        element.style.display = "none";
      });
      
    }
  }

  render() {
    return (
      <div className="content">
               <FileUpload></FileUpload>
      </div>
    );
  }
}

export default UserProfile;
